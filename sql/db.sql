DROP TABLE IF EXISTS `goods`;
CREATE TABLE IF NOT EXISTS `goods` (
  `Gno` varchar(20) NOT NULL COMMENT '商品编码（主键）',
  `name` varchar(20) NOT NULL COMMENT '商品名称',
  `category` varchar(16) NOT NULL COMMENT '商品类别（mobile/book）',
  `price` float NOT NULL COMMENT '单价',
  PRIMARY KEY (`Gno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE IF NOT EXISTS `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id（主键，auto_increment）',
  `Ono` varchar(20) NOT NULL COMMENT '订单号',
  `Gno` varchar(20) NOT NULL COMMENT '商品编码',
  `count` int(11) NOT NULL COMMENT '购买数量',
  `total` float NOT NULL COMMENT '总订单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单详细表';