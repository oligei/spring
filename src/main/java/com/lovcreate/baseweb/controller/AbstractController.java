package com.lovcreate.baseweb.controller;


import com.lovcreate.baseweb.response.BaseResponse;

/**
 * Created by dennis on 2016/11/4.
 */
public class AbstractController {


    public BaseResponse success(String message, Object data) {

        return new BaseResponse(0, message, data);
    }

    public BaseResponse success(String message) {

        return new BaseResponse(0, message);
    }


    public BaseResponse failure(String message, Object data) {

        return new BaseResponse(-1, message, data);
    }

    public BaseResponse failure(String message) {

        return new BaseResponse(-1, message);
    }

    public BaseResponse tokenFailure(String message) {

        return new BaseResponse(3, message);
    }


}
