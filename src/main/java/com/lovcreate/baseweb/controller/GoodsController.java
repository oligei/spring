package com.lovcreate.baseweb.controller;

import com.lovcreate.baseweb.entity.GoodsEntity;
import com.lovcreate.baseweb.service.GoodsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午5:28
 */
@RequestMapping("/goods")
@Controller
public class GoodsController {
    private final static Logger logger = LoggerFactory.getLogger(GoodsController.class);

    @Autowired
    private GoodsService goodsService;

    @RequestMapping("/one")
    @ResponseBody
    public Object queryByGno(@RequestParam String gno) {
        //要记得参数需要校验
        if (StringUtils.isBlank(gno)) {
            return "";
        }
        return goodsService.queryByGno(gno);
    }

    @RequestMapping("/list")
    @ResponseBody
    public Object query() {
        return goodsService.queryList();
    }

    /**
     * http://localhost:8080/goods/add?gno=1016&name=%E6%B5%AA%E6%BD%AE%E4%B9%8B%E5%B7%85&category=book&price=55
     *
     * @param goods
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public String add(GoodsEntity goods) {
        if (goodsService.add(goods)) {
            return "添加成功";
        }
        return "添加失败";
    }

}
