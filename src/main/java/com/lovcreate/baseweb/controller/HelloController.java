package com.lovcreate.baseweb.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午4:48
 */
@RequestMapping("/hello")
@Controller
public class HelloController {
    private final static Logger logger = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping(value = "/you", method = RequestMethod.GET)
    public void helloGet(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        try {
            response.getWriter().write(String.format("Hello %s!", StringUtils.isBlank(name) ? "world" : name));
        } catch (Exception e) {
            logger.error("异常了", e);
        }
    }

    @RequestMapping(value = "/youParam")
    public void helloPost(@RequestParam("name") String name, HttpServletRequest request, HttpServletResponse response) {
        try {
            response.getWriter().write(String.format("Hello %s!", StringUtils.isBlank(name) ? "world" : name));
        } catch (Exception e) {
            logger.error("异常了", e);
        }
    }


    @RequestMapping(value = "/you", method = RequestMethod.POST)
    public void helloPost(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        try {
            response.getWriter().write(String.format("Hello %s!", StringUtils.isBlank(name) ? "world" : name));
        } catch (Exception e) {
            logger.error("异常了", e);
        }
    }


}
