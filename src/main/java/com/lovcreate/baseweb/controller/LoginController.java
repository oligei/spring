package com.lovcreate.baseweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午9:41
 */
@RequestMapping("/login")
@Controller
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public String page(HttpServletRequest request, HttpServletResponse response) {
        return "login";
    }
}
