package com.lovcreate.baseweb.controller;

import com.google.common.collect.Lists;
import com.lovcreate.baseweb.service.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午6:02
 */
@RequestMapping("/order")
@Controller
public class OrderController {
    private final static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @RequestMapping("/submit")
    @ResponseBody
    public Object submit(String paramStr) {
        String[] splits = StringUtils.split(paramStr, "|");
        if (splits == null || splits.length < 2) {
            return "参数格式错误";
        }
        List<String[]> goodsList = Lists.newArrayList();
        for (int i = 1; i < splits.length; ++i) {
            goodsList.add(StringUtils.split(splits[i], " "));
        }
        if (orderService.submitOrder(splits[0], goodsList)) {
            return "下单成功";
        }
        return "下单失败";
    }
}
