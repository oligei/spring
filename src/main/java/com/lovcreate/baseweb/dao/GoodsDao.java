package com.lovcreate.baseweb.dao;

import com.lovcreate.baseweb.entity.GoodsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午5:09
 */
@Repository
public class GoodsDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public GoodsEntity queryByGno(String gno) {
        String sql = "SELECT gno,name,category,price FROM goods where gno = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{gno}, new BeanPropertyRowMapper<GoodsEntity>(GoodsEntity.class));
    }

    public List<GoodsEntity> queryList() {
        String sql = "SELECT gno,name,category,price FROM goods";
        return jdbcTemplate.queryForList(sql, new Object[]{}, GoodsEntity.class);
    }

    public int add(GoodsEntity goodsEntity) {
        String sql = "insert into goods(gno, name, category, price)values(?,?,?,?)";
        return jdbcTemplate.update(sql, goodsEntity.getGno(), goodsEntity.getName(), goodsEntity.getCategory(), goodsEntity.getPrice());
    }
}
