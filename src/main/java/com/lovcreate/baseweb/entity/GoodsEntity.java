package com.lovcreate.baseweb.entity;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午5:04
 */
public class GoodsEntity {
    /**
     * 商品编码（主键）
     */
    private String gno;
    /**
     * 商品名称
     */
    private String name;
    /**
     * 商品类别（mobile/book）
     */
    private String category;
    /**
     * 单价
     */
    private float price;

    public String getGno() {
        return gno;
    }

    public void setGno(String gno) {
        this.gno = gno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
