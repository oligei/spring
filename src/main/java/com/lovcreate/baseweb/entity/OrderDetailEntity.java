package com.lovcreate.baseweb.entity;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午6:15
 */
public class OrderDetailEntity {
    /**
     * id
     */
    private Integer id;
    /**
     * 订单号
     */
    private String ono;
    /**
     * 商品编码
     */
    private String gno;
    /**
     * 购买数量
     */
    private int count;
    /**
     * 总订单价
     */
    private float total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getOno() {
        return ono;
    }

    public void setOno(String ono) {
        this.ono = ono;
    }

    public String getGno() {
        return gno;
    }

    public void setGno(String gno) {
        this.gno = gno;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
