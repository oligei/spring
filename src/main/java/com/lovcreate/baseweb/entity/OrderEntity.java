package com.lovcreate.baseweb.entity;

import java.util.List;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午6:30
 */
public class OrderEntity {
    /**
     * 订单号（主键）
     */
    private String ono;
    /**
     * 下单用户编码
     */
    private String uno;
    /**
     * 下单时间
     */
    private String createTime;
    /**
     * 总订单价
     */
    private float total;
    /**
     * 订单详情
     */
    private List<OrderDetailEntity> detailList;

    public String getOno() {
        return ono;
    }

    public void setOno(String ono) {
        this.ono = ono;
    }

    public String getUno() {
        return uno;
    }

    public void setUno(String uno) {
        this.uno = uno;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public List<OrderDetailEntity> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<OrderDetailEntity> detailList) {
        this.detailList = detailList;
    }
}
