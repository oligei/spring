package com.lovcreate.baseweb.response;


import java.util.List;

/**
 * controller 返回基本样式定义
 * Created by dennis on 16/9/28.
 */
public class BaseResponse {

    private static final Double VERSION = 1.0;

    /**
     * 结果
     */
    private int code;

    /**
     * 返回信息,中文
     */
    private String msg;

    /**
     * 返回数据
     */
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BaseResponse(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;

        if (data instanceof List) {
            CollectionResponse response = new CollectionResponse();
            response.setList((List) data);
            this.data = response;
        } else {
            this.data = data;
        }

    }

    public BaseResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static Double getVERSION() {
        return VERSION;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BaseResponse() {

    }

    public class CollectionResponse {

        private List List;

        public java.util.List getList() {
            return List;
        }

        public void setList(java.util.List list) {
            List = list;
        }
    }

}
