package com.lovcreate.baseweb.service;

import com.lovcreate.baseweb.dao.GoodsDao;
import com.lovcreate.baseweb.entity.GoodsEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品相关
 * 查询 更改示例，可参考此处基本用法
 *
 * @author: zhangliang
 * @date: 2018/8/12 下午5:10
 */
@Service
public class GoodsService {
    private final static Logger logger = LoggerFactory.getLogger(GoodsService.class);

    @Autowired
    private GoodsDao goodsDao;

    public GoodsEntity queryByGno(String gno) {
        return goodsDao.queryByGno(gno);
    }

    public List<GoodsEntity> queryList() {
        return goodsDao.queryList();
    }

    public boolean add(GoodsEntity goodsEntity) {
        return goodsDao.add(goodsEntity) > 0;
    }
}
