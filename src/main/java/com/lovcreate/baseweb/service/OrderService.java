package com.lovcreate.baseweb.service;

import com.google.common.collect.Lists;
import com.lovcreate.baseweb.entity.GoodsEntity;
import com.lovcreate.baseweb.entity.OrderDetailEntity;
import com.lovcreate.baseweb.entity.OrderEntity;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author: zhangliang
 * @date: 2018/8/12 下午5:56
 */
@Service
public class OrderService {
    private final static Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private GoodsService goodsService;

    public boolean submitOrder(String uno, List<String[]> goodsList) {

        //TODO 查询用户信息 参考goods实现
        //TODO 创建订单对象
        OrderEntity order = new OrderEntity();
        order.setOno(generateOrderNo());
        order.setDetailList(Lists.newArrayListWithCapacity(goodsList.size()));
        order.setTotal(0f);
        //解析商品信息
        for (String[] goods : goodsList) {
            GoodsEntity goodsEntity = goodsService.queryByGno(goods[0]);
            OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
            orderDetailEntity.setGno(goodsEntity.getGno());
            orderDetailEntity.setCount(Integer.parseInt(goods[1]));
            orderDetailEntity.setOno(order.getOno());
            orderDetailEntity.setTotal(Integer.parseInt(goods[1]) * goodsEntity.getPrice());
            order.getDetailList().add(orderDetailEntity);
            //todo 计算订单总价
        }
        //todo 校验订单金额与账户余额
        //todo 保存订单
        //todo 保存订单详细
        //todo 更新账户金额
        return true;
    }

    private String generateOrderNo() {
        return DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS");
    }
}
