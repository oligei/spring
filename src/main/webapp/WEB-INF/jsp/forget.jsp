<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/8
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>忘记密码</title>
    <link rel="stylesheet" href="../../fe/css/forgetcss.css">
</head>
<body>
<div class="container">
    <div>
        <h2>忘记密码</h2>
        <div class="content">
            <label class="inputcontent" for="number">手机号码</label>
            <input type="text" id="number" name="number" class="inputset">
        </div>
        <div>
            <input type="button" id="submitbutton" class="submitbutton" value="确定">
        </div>
    </div>
</div>
<script src="jquery-3.3.1.min.js"></script>
<script>
    $('#submitbutton').click(function () {
        var number = $('#number').val();
        $.post('/forget', {number: number}, function (resp) {
            if (resp === "done") {
                window.location.href = "/reset.jsp";
            } else {
                alert(resp);
            }
        })
    });
</script>
</body>
</html>