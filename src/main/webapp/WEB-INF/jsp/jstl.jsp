<%--
  Created by IntelliJ IDEA.
  User: shizhenwei
  Date: 2018/8/3
  Time: 上午10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:set var="flag" scope="session" value="${true}"/>
<c:set var="when" scope="session" value="${1}"/>
<c:if test="${flag}">
    <span>我只有在正确的时候才会显示</span>
</c:if>
<br/>
<c:choose>
    <c:when test="${when == 1}">
        <span>我是等于1的那个</span>
    </c:when>
    <c:when test="${when == 2}">
        <span>我是等于2的那个</span>
    </c:when>
    <c:when test="${when == 3}">
        <span>我是等于3的那个</span>
    </c:when>
    <c:otherwise>
        <span>我是都不对的那个</span>
    </c:otherwise>
</c:choose>
<br/>
<c:forEach var="i" begin="1" end="5">
    <span>Item <c:out value="${i}"/></span>
</c:forEach>
<br/>
<h3>日期格式化:</h3>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<p>原始日期格式：${now}</p>
<p>日期格式化 (7): <fmt:formatDate pattern="yyyy-MM-dd"
                              value="${now}"/></p>
</body>
</html>
