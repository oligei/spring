<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/8
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" href="/fe/css/logincss.css">
</head>
<body>
<div class="container">
    <h2>登&nbsp;&nbsp;录</h2>
    <div class="tip">
        <label class="text">请妥善保存登录信息</label>
    </div>
    <div class="form">
        <div>
            <label class="inputcontent" for="username">用户名</label>
            <input type="text" id="username" name="username" class="inputset">
        </div>
        <div class="content">
            <label class="inputcontent" for="password">密码</label>
            <input type="password" id="password" name="password" class="inputset">
        </div>
        <div class="click">
            <a href="forget.jsp" class="forgetpassword">忘记密码？</a>
            <a href="regist.jsp" class="register">没有账号？注册</a>
        </div>
        <div class="click">
            <input type="button" id="submitButton" class="submitButton" value="登录">
        </div>
    </div>
</div>
<script src="/fe/js/jquery-3.3.1.min.js"></script>
<script>
    $('#submitButton').click(function () {
        var username = $('#username').val();
        var password = $('#password').val();
        $.post('/login', {username: username, password: password}, function (resp) {
            if (resp == 0) {
                window.location.href = "/shop.jsp";
            } else {
                $('.text').text(resp);
            }
        })
    });
</script>
</body>
</html>