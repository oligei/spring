<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/8
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>注册</title>
    <link rel="stylesheet" href="../../fe/css/registercss.css">
</head>
<body>
<div class="container">
    <h2>注&nbsp;&nbsp;册</h2>
    <div class="formset">
        <div class="content">
            <label class="inputcontent" for="username">用户名</label>
            <input type="text" id="username" name="username" class="inputsetun">
            <div class="unametip"></div>
        </div>
        <div class="content">
            <label class="inputcontent" for="realname">真实姓名</label>
            <input type="text" id="realname" name="realname" class="inputsetrn">
            <div class="rnametip"></div>
        </div>
        <div class="content">
            <label class="inputcontent">性别</label>
            <div class="radiosex">
                <input class="radio" type="radio" id="male" name="sex" value="man" checked>
                <label class="radiocontent" for="male">男</label>
                <input class="radio" type="radio" id="female" name="sex" value="woman">
                <label class="radiocontent" for="female">女</label>
                <div class="sextip"></div>
            </div>
        </div>
        <div class="content">
            <label class="inputcontent" for="number">手机号码</label>
            <input type="text" id="number" name="number" class="inputsetn">
            <div class="numbertip"></div>
        </div>
        <div class="content">
            <label class="inputcontent" for="password">密码</label>
            <input type="password" id="password" name="password" class="inputsetp">
            <div class="passwordtip"></div>
        </div>
        <div class="click">
            <a href="login.jsp" class="login">已有账号？登录</a>
        </div>
        <div class="click">
            <input type="reset" class="resetbutton" value="重置">
            <input type="button" id="submitbutton" class="submitbutton" value="注册">
        </div>
    </div>
</div>
<script src="jquery-3.3.1.min.js"></script>
<script>
    $('#submitbutton').click(function (e) {
        var username = $('#username').val();
        var realname = $('#realname').val();
        var sex = $("input[type='radio']:checked").val();
        var number = $('#number').val();
        var password = $('#password').val();
        var flag = true;
        if (username == "") {
            $('.unametip').text("用户名不能为空");
            $('.inputsetun').css('border-color', 'red');
            flag = false;
        } else {
            $('.unametip').text("");
            $('.inputsetun').css('border-color', '#ccc');
        }
        if (realname == "") {
            $('.rnametip').text("真实姓名不能为空");
            $('.inputsetrn').css('border-color', 'red');
            flag = false;
        } else {
            $('.rnametip').text("");
            $('.inputsetrn').css('border-color', '#ccc');
        }
        if (sex == null) {
            $('.sextip').text("性别不能为空");
            flag = false;
        } else {
            $('.sextip').text("");
        }
        if (number == "") {
            $('.numbertip').text("电话号码不能为空");
            $('.inputsetn').css('border-color', 'red');
            flag = false;
        } else {
            $('.numbertip').text("");
            $('.inputsetn').css('border-color', '#ccc');
        }
        if (password == "") {
            $('.passwordtip').text("密码不能为空");
            $('.inputsetp').css('border-color', 'red');
            flag = false;
        } else {
            $('.passwordtip').text("");
            $('.inputsetp').css('border-color', '#ccc');
        }
        $.post('/regist', {
            username: username,
            realname: realname,
            sex: sex,
            number: number,
            password: password
        }, function (resp) {
            if (resp === "done") {
                if (flag) {
                    window.location.href = "/login.jsp";
                }
            }
        })
    });
</script>
</body>
</html>