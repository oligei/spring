<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/8
  Time: 11:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>重置密码</title>
    <link rel="stylesheet" href="../../fe/css/css.css">
</head>
<body>
<div class="container">
    <h2>重置密码</h2>
    <div class="formset">
        <div>
            <label class="inputcontent" for="newpassword">新密码</label>
            <input type="password" id="newpassword" name="newpassword" class="inputset">
        </div>
        <div>
            <label class="inputcontent" for="confirm">确认密码</label>
            <input type="password" id="confirm" name="confirm" class="inputset">
        </div>
        <div>
            <input type="button" id="submitbutton" class="submitbutton" value="提交">
        </div>
    </div>
</div>
<script src="jquery-3.3.1.min.js"></script>
<script>
    $('#submitbutton').click(function () {
        var newpassword = $('#newpassword').val();
        var confirm = $('#confirm').val();
        $.post('/reset', {newpassword: newpassword, confirm: confirm}, function (resp) {
            if (resp === "done") {
                window.location.href = "/login.jsp";
            } else {
                alert(resp);
            }
        })
    });
</script>
</body>
</html>